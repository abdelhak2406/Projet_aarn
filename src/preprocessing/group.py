"""ce script a pour but de grouper les 2 dataset.

base covid et base nutrition
puis convertit en un encodage binaire ou decimal 

Nous suivons la convention  `pydocstyle`
"""
# %%
import pandas as pd
import numpy as np
from pandas.core.frame import DataFrame
from pandas.core.reshape.merge import merge

PATH_DATA = "../../data/"

def read_data():
    """Read the data."""
    cov_db = pd.read_csv(PATH_DATA+"after/covid.csv")
    nut_db = pd.read_csv(PATH_DATA+"after/nutritients.csv")
    return cov_db,nut_db


def add_0(bin_str, max_len):
    """Add k 0's to binary strings to get a certain length."""
    num_0 = max_len  - len(bin_str)
    return num_0*'0'+bin_str


def encode_special_binary(dataframe, column_name)-> pd.DataFrame:
    """Encode a column of dataframe into binary with ; separated.
 
    Keyword arguments:
    dataframe --  the dataframe we want the column_name to be encoded in binary 
    column_name -- the name of the column that needs to be converted into binary
                    note that the dataType of the column needs to be integer,
                    otherwise they might be some problems  
    Returns:
     the dataframe with the column_name encoded in ";" separated binary 
     
    Note:
     if we have 1 and the max_number of column_name is 4 then we will
     encode it like 0;0;0;1
     
    """
    dataframe[column_name] = dataframe[column_name].apply(lambda x :bin(x)[2:] )
    #get the max_length of the string in the column column_name 
    max_len = dataframe[column_name].map(len).max()
    dataframe[column_name] = dataframe[column_name].apply(lambda x : add_0(x,max_len))
    #here we have in binary but not like 0;0;0;0;1 so we will create it
    #had to comment this so its easier for me to create new columns for every bits and copy its values
    #dataframe[column_name] = dataframe[column_name].apply(lambda b : ';'.join(b[i:i + 1] for i in range(0, len(b), 1)))
    return dataframe

def encode_category_number(dataframe:pd.DataFrame, column_name:str)->pd.DataFrame : 
    """Encode a column of dataframe into decimal.
    
    Keyword arguments:
        dataframe -- the dataframe we want the column_name to be encoded in binary 
        column_name -- the name of the column that needs to be converted into binary
                    note that the dataType of the column might be a problem in some cases 
    Returns:
        dataframe -- the dataframe with the column_name encoded in decimal
    
    """
    dataframe[column_name] = dataframe[column_name].astype('category') 
    dataframe[column_name] = dataframe[column_name].cat.codes
    return dataframe

def add_quotion_pop_deaths(merged:pd.DataFrame):
    """Generete the Nbcoviddeaths/size_population for each line(Country)and add it to merge dataset.
    
    Keyword arguments:
    merged -- the dataframe that contains the merged elements  
    
    Returns:
    dataframe -- the dataframe with the nb_deaths/nb_popu added
    
    Note:
        The `country` column in the Merged dataset needs to be encoded in the iso 3166-1 alpha-3 stadard 
        not in integer or any other encoding. 
        
    """
    path_covid_df= PATH_DATA+"before/data.csv"
    covid_data = pd.read_csv(path_covid_df) 
    merged["nb_death_cov_div_nb_population"] = None
    for i in merged.Country.unique():
        taill_pop = covid_data["popData2019"][covid_data.countryterritoryCode==str(i)].iloc[0]
        deaths = float(merged.loc[merged.Country==str(i),"Deaths"]) 
        merged["nb_death_cov_div_nb_population"][merged.Country==str(i)]= deaths/taill_pop
    
    return merged 
    
    
def create_multicolumn_binary(dataframe:pd.DataFrame, column_name:str)->pd.DataFrame:
    """Add one column for every bit of the column_name column."""
    max_len = dataframe[column_name].map(len).max()
    for i in range(max_len):
        dataframe["bit"+str(i)]=0

    for i in range(len(dataframe)) :
        for j in range(max_len):
            binaryDeaths=dataframe.at[i,column_name]
            dataframe.at[i,"bit"+str(j)]=binaryDeaths[j]

    return dataframe




def main():
    """Main."""
    global cov_db,nut_db
    cov_db,nut_db = read_data()
    global merged 
    merged = pd.merge(cov_db,nut_db,on='Country',how='inner')
    merged =  add_quotion_pop_deaths(merged)
    merged=encode_special_binary(merged,'Deaths')
    merged=create_multicolumn_binary(merged,"Deaths")
   
    #save data 
    merged.to_csv(PATH_DATA+"after/merged.csv",index=False,header=True)
if __name__ == "__main__":
    main()
# %%
