"""Script qui fait le pretraitement de la base de donne covid et generer <base Covid>."""
# %%
import pandas as pd
path_data= "../../data/before/data.csv"
path_save = "../../data/after/"

data=pd.read_csv(path_data)
# we will only keep the 3 columns below 
frame=data.loc[:,["countryterritoryCode","cases","deaths"]]

countries=frame["countryterritoryCode"].unique()
listD=[]#list deaths
listC=[]#list contaminations
s=0
c=0
# loop through every country and compute   the number of deaths and cases

for country in countries :
    j=frame[frame["countryterritoryCode"]==country]
    # count number of deaths ad cases for every country
    for x in range(len(j)):
        s+=j["deaths"].iat[x]
        c+=j["cases"].iat[x]
    listD.append(s)
    listC.append(c)
    s=0
    c=0

covidCases= pd.DataFrame(
    {
        "Country" : countries,
        "Cases" : listC,
        "Deaths" : listD
    }

)

print(covidCases)

#sauvegarder le dataset
covidCases.to_csv(path_save +"covid.csv",index=False,header=True)

# %%
