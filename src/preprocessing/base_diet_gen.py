"""Script qui fait le pretraitement de la base de donne Global diary database et qui genere <base Diet>."""
# %%
import pandas as pd
path_data= "../../data/before/gdd-final-estimate_06022021/Country-level estimates/"
path_save = "../../data/after/"
def median_data(path):
    """Read the dataframe and the median data."""
    dataFrame = pd.read_csv(path_data+path)
    frame = dataFrame["median"]
    return frame


list_csv = {"v03_cnty.csv", "v04_cnty.csv", "v05_cnty.csv", "v06_cnty.csv", "v07_cnty.csv",
            "v08_cnty.csv", "v09_cnty.csv", "v10_cnty.csv", "v11_cnty.csv", "v12_cnty.csv", "v13_cnty.csv",
            "v14_cnty.csv", "v15_cnty.csv", "v16_cnty.csv", "v17_cnty.csv", "v18_cnty.csv", "v23_cnty.csv",
            "v34_cnty.csv", "v57_cnty.csv"}


data1 = pd.read_csv(path_data+'v03_cnty.csv')
#iso3  correspond a pays (Country)
nutritients = pd.DataFrame({
    "iso": data1["iso3"]
})
# %%
#  add nutrimen column to the nutrinents dataframe 
# for each nutrinent we will add the mean daily intake 
for path in list_csv:
    median = median_data(path)
    nutritients[path[:3]] = median
# %%

countries = data1["iso3"].unique()
final_nut = pd.DataFrame({
    "Country": countries
})
for column in nutritients.columns:
    final_nut[column] = ""
i = 0
for country in countries:
    spec = nutritients[nutritients["iso"] == country]
    for column in nutritients.columns:
        if(column != 'iso'):
            final_nut[column].iat[i] = (spec[column].sum())/len(spec)
    i += 1

final_nut.drop("iso", axis=1, inplace=True)
final_nut.to_csv(path_save+"nutritients.csv", index=False, header=True)
