trainFcs = {'trainrp', 'trainscg', 'traincgp', 'trainlm'};
cas={10,5,25,10,50,25};

[trainP,valP,testP,trainInd,valInd,testInd] = dividerand(input,0.6,0.2,0.2);
[trainT,valT,testT] = divideind(target,trainInd,valInd,testInd);

filename = 'resultats.txt';
filepath = cd;
file     = fullfile(filepath, filename);
fileID=fopen(file,'wt');
formatSpec = 'training function : %s, performance: %f, best epoch : %i \n';

best_perf=100000000000000000000;
best_train='';best_param='';

j=1;
while j<length(cas) 
    fprintf (fileID,'hidden layers : 2, nbneurons: [%d,%d],\n',cas{j},cas{j+1});
    for i = 1:length(trainFcs) 

        covid_net=feedforwardnet([cas{j},cas{j+1}],trainFcs{i});
        [net,tr] = train(covid_net,[trainP,valP,testP],[trainT,valT,testT]);
        y = net(input) ;
        plotregression(y,target)
        
        fprintf(fileID,formatSpec,trainFcs{i},tr.best_vperf,tr.best_epoch);

        if best_perf > tr.best_vperf
            best_perf=tr.best_vperf;
            best_train=trainFcs{i};
            best_param={cas{j},cas{j+1}};
        end
    end
    j=j+2;
end

fprintf(fileID,'\nBest parameters :[%d,%d]\n best training function : %s,\n best performance :%.2f'...
    ,best_param{1},best_param{2},best_train,best_perf);
fclose(fileID);