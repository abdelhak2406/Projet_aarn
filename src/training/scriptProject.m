trainFcs = {'trainrp', 'trainscg', 'traincgp', 'trainlm'};
[trainP,valP,testP,trainInd,valInd,testInd] = dividerand(input,0.6,0.2,0.2);
[trainT,valT,testT] = divideind(target,trainInd,valInd,testInd);
fileID=fopen('resultats.txt','w');
formatSpec = 'training function : %s, performance: %.2f, best epoch : %i \n';
fprintf (fileID,'hidden layers : 2, nbneurons: 50,\n');
best_perf=100000000000000000000;
best_train='';

for i = 1:length(trainFcs) 
    
    covid_net=feedforwardnet(50,trainFcs{i});
    [net,tr] = train(covid_net,[trainP,valP,testP],[trainT,valT,testT]);
    fprintf(fileID,formatSpec,trainFcs{i},tr.best_vperf,tr.best_epoch);
    
    if best_perf > tr.best_vperf
        best_perf=tr.best_vperf;
        best_train=trainFcs{i};
    end
end

fprintf(fileID,'\nbest training function : %s, best performance : %.2f',best_train,best_perf);

fclose(fileID);